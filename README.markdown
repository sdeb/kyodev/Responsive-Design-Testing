# Responsive Design Testing

This tool is for everyone who needs a quick and easy way to test their website design in multiple screen widths.

## Permalink style testing

You can test any website, and provide the link to anyone you like by adding their URL to the end of the testing page address.
For example:
http://mattkersley.com/responsive?google.com

## Installing on your own server

- Copy index.html and responsive.js onto your machine
- Update the *defaultURL* at the top of responsive.js to your own website
- Upload the files into a subdirectory on your server
- Navigate to the new subdirectory via a browser

Once you've uploaded, you can navigate your website from within the iframes, and the others will update.
This won't work for external sites however due to browser security restrictions.


## fork 

c'est [un fork rapide de](https://github.com/mattkersley/Responsive-Design-Testing) pour avoir un scroll vertical (et non horizontal) des iframes 


## http://

**utiliser http://**  (pas de https:// ni de file://)


## license

license initiale inconnue. dans la mesure du possible, license des modifications: [LPRAB / WTFPL](https://gitlab.com/sdeb/nstest/blob/master/LICENSE.md#lprab-wtfpl)  

![license LPRAB / WTFPL](https://img.shields.io/badge/license-LPRAB%20%2F%20WTFPL-blue.svg?longCache=true&style=for-the-badge)
